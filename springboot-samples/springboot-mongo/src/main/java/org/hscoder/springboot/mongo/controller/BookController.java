package org.hscoder.springboot.mongo.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.hscoder.springboot.mongo.domain.Book;
import org.hscoder.springboot.mongo.repository.BookRepository;
import org.hscoder.springboot.mongo.service.BookService;
import org.hscoder.springboot.simplebuild.util.DateUtil;
import org.hscoder.springboot.simplebuild.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/book")
@Slf4j
public class BookController {
    @Autowired
    private BookService bookService;

    @Autowired
    private BookRepository bookRepository;

    private final String category = "novel";
    private final String title = "the three musketeers";
    private final String author = "Alexandre Dumas";

    private List<Book> preInsertedBooks;

    @Autowired
    Environment environment;

    @RequestMapping("/env")
    public String getActiveProfiles() {

        System.out.println("-------------");
        for (final String profileName : environment.getActiveProfiles()) {
            System.out.println("Currently active profile - " + profileName);
        }
        return "ok";
    }

    @RequestMapping("/init")
    public String initData() {

        getActiveProfiles();
        preInsertedBooks = new ArrayList<Book>();

        Book book1 = new Book();

        book1.setAuthor(author);
        book1.setTitle(title);
        book1.setCategory(category);
        book1.setPrice(60);
        book1.setPublishDate(DateUtil.toDate("2016-11-09"));

        book1.setVoteCount(10);
        book1.setCreateTime(new Date());
        book1.setUpdateTime(book1.getCreateTime());

        Book book2 = new Book();

        book2.setAuthor(author);
        book2.setTitle("The New World");
        book2.setCategory(category);
        book2.setPrice(30);
        book2.setPublishDate(DateUtil.toDate("2016-12-09"));

        book2.setVoteCount(8);
        book2.setCreateTime(new Date());
        book2.setUpdateTime(book1.getCreateTime());

        preInsertedBooks.add(bookRepository.save(book1));
        preInsertedBooks.add(bookRepository.save(book2));

        Map res=new HashMap<>();
        res.put("code","10000");
        res.put("msg","操作成功");
        res.put("data",preInsertedBooks);
        log.info("res={}",JSONObject.toJSONString(res));
        return JSONObject.toJSONString(res);
    }

    @RequestMapping("/create")
    public String create() {
        Map res = new HashMap<>();
        try {
            Book newBook = bookService.createBook(category, "New Book", author, 30, new Date());
            res.put("code", "10000");
            res.put("msg", "操作成功");
            res.put("data", newBook);
        }catch(Exception e){
            res.put("code", "-11111");
            res.put("msg", "操作失败,"+e.getMessage());
        }
        log.info("res={}",JSONObject.toJSONString(res));
        return JSONObject.toJSONString(res);
    }

    @RequestMapping(value = "/del",method = RequestMethod.POST)
    public String delete(String id) {
        Map res = new HashMap<>();
        try {
            Book book = bookRepository.findOne(id);
            if(book == null){
                throw new Exception("没有该数据");
            }
            boolean result = bookService.deleteBook(id);
            res.put("code", "10000");
            res.put("msg", "操作成功,"+result);
        }catch(Exception e){
            res.put("code", "-11111");
            res.put("msg", "操作失败,"+e.getMessage());
        }
        log.info("res={}",JSONObject.toJSONString(res));
        return JSONObject.toJSONString(res);
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public String update(String id) {
        Map res = new HashMap<>();
        try{
            Book book = bookRepository.findOne(id);
            if(book == null){
                throw new Exception("没有该数据");
            }
            log.info("oldPrice={}",book.getPrice());
            boolean result = bookService.updatePrice(book.getId(), 100);
            log.info("update={}",result);
            book = bookRepository.findOne(book.getId());
            log.info("newPrice={}",book.getPrice());
            res.put("code", "10000");
            res.put("msg", "操作成功,"+result);
            res.put("data", book);
        }catch(Exception e){
            res.put("code", "-11111");
            res.put("msg", "操作失败,"+e.getMessage());
        }
        log.info("res={}",JSONObject.toJSONString(res));
        return JSONObject.toJSONString(res);
    }

    @RequestMapping(value = "/getByTitle",method = RequestMethod.POST)
    public String getBookByTitle(String id) {
        Map res = new HashMap<>();
        try{
            Book book = bookService.getBookByTitle(title);
            if(book == null){
                throw new Exception("没有该数据");
            }
            log.info("1.book={}",book);
            List<Book> books = bookService.listTopVoted(category, 10);
            log.info("2.books={}",books);

            boolean result = bookService.addVote(id, 10);
            log.info("3.result={}",result);
            // must change the rank
            List<Book> books2 = bookService.listTopVoted(category, 10);
            log.info("4.book={}",books2);

            PageResult<Book> search = bookService.search(category, null, author, DateUtil.toDate("2016-12-01"),
                    DateUtil.toDate("2016-12-30"), 0, 20);
            log.info("5.search={}",search);

            res.put("code", "10000");
            res.put("msg", "操作成功");
            res.put("data", book);
        }catch(Exception e){
            res.put("code", "-11111");
            res.put("msg", "操作失败,"+e.getMessage());
        }
        log.info("res={}",JSONObject.toJSONString(res));
        return JSONObject.toJSONString(res);
    }

}
